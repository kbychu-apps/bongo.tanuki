# Bongo Tanuki
<p align="center">
  <img src="/meta/thumbnail.png">
</p>
Hit the bongos like Bongo Tanuki! http://bongo.tanuki.services

## About the meme
Bongo Tanuki is a fork of [Bongo Cat](https://bongo.cat) 

## Getting started
Start a simple web server.

On the Mac, run the following

`python -m SimpleHTTPServer 8000`

## Song Examples
Happy Birthday to You
> 1 1 3 1 6 5\
1 1 3 1 8 6

Ode to Joy - Friedrich Schiller
> 5 5 6 8 8 6 5 3 1 1 3 5 5 3 3\
5 5 6 8 8 6 5 3 1 1 3 5 3 1 1\
3 3 5 1 3 5 6 5 1 3 5 6 5 3 1 3 1

In the End - Linkin Park
> 3 0 0 6 5 5 5 5 6 3\
0 0 6 5 5 5 5 6 3\
C\
Space\
0 0 6 5 5 5 5 6 3\
0 0 6 5 5 5 5 6 3\
C

Come as You Are - Nirvana
> 1 1 2 3\
6 3 6 3 3 2 1 8 1 1 8 1 2 3

[Browse more songs](https://github.com/Externalizable/bongo.cat/issues?utf8=%E2%9C%93&q=is%3Aissue+label%3A%22Type%3A+Song+Submission%22+) or [submit your own](https://github.com/Externalizable/bongo.cat/issues/new)!

## Featured in

## Built with
- [lowLag.js](https://lowlag.alienbill.com/) - To play lag free sounds on a wide range of devices
- [SoundManager 2](http://www.schillmania.com/projects/soundmanager2/) - Used by lowLag.js

## Authors

## Thanks
- Tanuki Growl from https://www.youtube.com/watch?v=W1zuPnEzA-o

## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
